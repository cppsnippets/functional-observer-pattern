//
// Created by Özen Özkaya on 3.07.2020.
//

#include "myObservableClass.h"
#include <iostream>

MyObservableClass::MyObservableClass() {

};

MyObservableClass::~MyObservableClass() {

};

observerId_t MyObservableClass::addListener(eventNotifyCallback_t cb, observableEvent_t eventID) {
    const auto id = observerId_t(++idValue());
    _callbacksMap.emplace(id, std::move(cb));
    _eventInterestMap.emplace(id,eventID);
    std::cout << "added a listener (observer) with id: " << id << std::endl;
    return id;
}

bool MyObservableClass::removeListener(const observerId_t id) {
    const auto itCB = _callbacksMap.find(id);
    if(itCB == _callbacksMap.end()){
        std::cout << "cannot remove listener (observer) with id: " << id <<" because it does not exist" << std::endl;
        return false;
    }
    _callbacksMap.erase(itCB);

    const auto itEvt = _eventInterestMap.find(id);
    if(itEvt == _eventInterestMap.end()){
        std::cout << "cannot remove listener (observer) with id: " << id <<" because its event does not exist" << std::endl;
        return false;
    }
    _eventInterestMap.erase(itEvt);
    std::cout << "removed a listener (observer) with id: " << id << std::endl;
    return true;
}

void MyObservableClass::doSomeObservableThingsForEventA() {
    notifyAll(EVENT_A);
}

void MyObservableClass::doSomeObservableThingsForEventB() {
    notifyAll(EVENT_B);
}

void MyObservableClass::notifyAll(observableEvent_t eventID) const {
    std::cout << "notifying all listeners (observers) for event: " << eventID << std::endl;
    for( const auto& pair : _callbacksMap ){
        genericMap_t<observerId_t, observableEvent_t>::const_iterator _eventTypeInterestItem = _eventInterestMap.find(pair.first);
        if(_eventTypeInterestItem->second == eventID){
            pair.second(eventID);
        }
    }
}