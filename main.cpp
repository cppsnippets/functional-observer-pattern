//
// Created by Özen Özkaya on 3.07.2020.
//
#include "myObservableClass.h"
#include <iostream>

void EventACallback(observableEvent_t evt){
    std::cout<< "Event A notification is received: " << evt << std::endl;
};

void EventBCallback(observableEvent_t evt){
    std::cout<< "Event B notification is received: " << evt << std::endl;
};

int main(){
    std::unique_ptr<MyObservableClass> myObservableClass = std::make_unique<MyObservableClass>();

    myObservableClass->addListener(
            ([](observableEvent_t evt){EventACallback(evt);}),
            EVENT_A
            );

    myObservableClass->addListener(
            ([](observableEvent_t evt){EventBCallback(evt);}),
            EVENT_B
    );

    myObservableClass->doSomeObservableThingsForEventA();
    myObservableClass->doSomeObservableThingsForEventB();

    return 0;
}
