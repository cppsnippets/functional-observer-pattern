//
// Created by Özen Özkaya on 3.07.2020.
//

#ifndef FUNCTIONAL_OBSERVER_PATTERN_MYOBSERVABLECLASS_H
#define FUNCTIONAL_OBSERVER_PATTERN_MYOBSERVABLECLASS_H

#include "observerInterface.h"

class MyObservableClass : public ObserverInterface{


public:
    MyObservableClass();
    ~MyObservableClass();

    observerId_t addListener(eventNotifyCallback_t cb, observableEvent_t eventID) override;
    bool removeListener(const observerId_t id) override;

    void doSomeObservableThingsForEventA();
    void doSomeObservableThingsForEventB();

private:
    void notifyAll(observableEvent_t eventID) const override;
    static size_t& idValue(){static size_t the_id; return the_id;};
    genericMap_t<observerId_t, eventNotifyCallback_t> _callbacksMap;
    genericMap_t<observerId_t, observableEvent_t> _eventInterestMap;
};

#endif //FUNCTIONAL_OBSERVER_PATTERN_MYOBSERVABLECLASS_H
