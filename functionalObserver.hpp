//
// Created by Özen Özkaya on 3.07.2020.
//

#ifndef FUNCTIONALOBSERVER_HPP
#define FUNCTIONALOBSERVER_HPP

#include <functional>
#include <unordered_map>
#include <utility>
#include <vector>

#include "events.h"

using eventNotifyCallback_t = std::function<void(observableEvent_t)>;
using eventNotifyFeedback_t = std::function<void(observableEvent_t, void*)>;
template < class Key, class Value > using genericMap_t = std::unordered_map<Key,Value>;
enum observerId_t{};

#endif //IMPORTEXPORT_FUNCTIONALOBSERVER_HPP
