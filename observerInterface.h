//
// Created by Özen Özkaya on 3.07.2020.
//
#ifndef _H_OBSERVER_INTERFACE
#define _H_OBSERVER_INTERFACE

#include "functionalObserver.hpp"

class ObserverInterface
{

public:

    virtual ~ObserverInterface(){};
    /**
     * @brief addListener
     *  do not miss to be notified when a significant change in network interfaces occured, use addListener!
     *  you can pass your callback and also your serviceTypeId (ex: 1 for syslog curently) and enjoy being notified
     *  when network interface config for sinec ins changed. bonus: same is possible for other services
     *  [you are welcome]
     *  how to use:
            NetworkInterfacesHandler networkInterfacesHandler(..);

            const Id listener_id_1 = networkInterfacesHandler.addListener( [&](sinecINS::networkInterfaces::serviceTypeID_t){ one.notify(); } , myDesiredServiceTypeId );
            const Id listener_id_2 = networkInterfacesHandler.addListener( [&](sinecINS::networkInterfaces::serviceTypeID_t){ two.notify(); },  mySecondDesiredServiceTypeId);
    */
    virtual observerId_t addListener( eventNotifyCallback_t cb , observableEvent_t eventID) { return observerId_t(0); };
    virtual observerId_t addListener( eventNotifyFeedback_t cb , observableEvent_t eventID) { return observerId_t(0); };

    /**
     * @brief removeListener
     * remove a listener callback from observer mechanism.
     * do not forgot the id that you added, otherwise you will be in trouble...
     * how to use:
     *      removeListener(listener_id_1);
     */
    virtual bool removeListener( const observerId_t id ) = 0;

private:
    /**
     * @brief notifyAll
     * notifies all the observables
     */
    virtual void notifyAll(observableEvent_t eventID) const {};

    /**
     * @brief notifyAll
     * notifies all the observables
     */
    virtual int notifyAll(observableEvent_t eventID, void*) const {return 0;};

};

#endif
